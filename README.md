# Graylog UPS Gateway

Original script by Lennart Koopmann: https://github.com/lennartkoopmann/ups-to-graylog

For this to work, you have to get the output of `apcaccess` look like this:

```
APC      : 001,038,0980
DATE     : 2021-04-30 16:04:23 +0200
HOSTNAME : 1150-ups
VERSION  : 3.14.14 (31 May 2016) debian
UPSNAME  : 1150-ups
CABLE    : USB Cable
DRIVER   : USB UPS Driver
UPSMODE  : Stand Alone
STARTTIME: 2021-04-17 11:22:36 +0200
MODEL    : Back-UPS RS  900MI
STATUS   : ONLINE
LINEV    : 232.0 Volts
LOADPCT  : 24.0 Percent
BCHARGE  : 96.0 Percent
TIMELEFT : 27.1 Minutes
MBATTCHG : 5 Percent
MINTIMEL : 3 Minutes
MAXTIME  : 0 Seconds
SENSE    : Medium
LOTRANS  : 176.0 Volts
HITRANS  : 288.0 Volts
ALARMDEL : 30 Seconds
BATTV    : 13.4 Volts
LASTXFER : Unacceptable line voltage changes
NUMXFERS : 2
XONBATT  : 2021-04-27 05:30:36 +0200
TONBATT  : 0 Seconds
CUMONBATT: 11 Seconds
XOFFBATT : 2021-04-27 05:30:37 +0200
LASTSTEST: 2021-04-20 10:44:53 +0200
SELFTEST : NO
STATFLAG : 0x05000008
SERIALNO : 4B0000P00000
BATTDATE : 2019-07-22
NOMINV   : 230 Volts
NOMBATTV : 12.0 Volts
NOMPOWER : 540 Watts
FIRMWARE : 948.g3 .I USB FW:g3
END APC  : 2021-04-30 16:04:26 +0200
```

# Variables to change

In the Ruby script, you have to adapt 

* Change the `apcaccess` command to reflect tool you use.
* Change the `GELF::Notifier` to point to a Graylog GELF UDP input.

# Running as Systemd service

This repository contains a systemd service script.
