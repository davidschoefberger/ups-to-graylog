require 'gelf'

# Change this to the address of a Graylog GELF UDP input.
n = GELF::Notifier.new("graylog.int.dsnet.io", 12201)

loop do
  begin
    # Change this to reflect the name that you gave your UPS in the NUT configuration.
    data = `apcaccess`
    
    fields = {}
    
    data.each_line do |d|
        if d.include? ":"
            parts = d.split(':')
    
            key = parts[0].strip
            value = parts[1].strip
    
            case key
            when 'STATUS'
                fields['status'] = value
            when 'LOADPCT'
                fields['load'] = value.to_i
            when 'MODEL'
                fields['device_model'] = value
            when 'SERIALNO'
                fields['device_serial'] = value
            when 'LINEV'
                fields['input_voltage'] = value.to_i
            when 'BATTV'
                fields['battery_voltage'] = value.to_i
            when 'TIMELEFT'
                fields['battery_runtime_minutes'] = value.to_i
            when 'BCHARGE'
                fields['charge_percent'] = value.to_i
            when 'LASTXFER'
                fields['last_switch_reason'] = value
            when 'NUMXFERS'
                fields['switch_count'] = value.to_i
            end
        end
    end
    
    n.notify!(
        :short_message => "UPS [#{fields['device_model']}/#{fields['device_serial']}] Battery charge: #{fields['charge_percent']}% @ Load #{fields['load']}%",
        :_status => fields['status'],
        :_load => fields['load'],
        :_model => fields['model'],
        :_serial => fields['serial'],
        :_input_voltage => fields['input_voltage'],
        :_battery_voltage => fields['battery_voltage'],
        :_battery_runtime_minutes => fields['battery_runtime_minutes'],
        :_charge_percent => fields['charge_percent'],
        :_last_switch_reason => fields['last_switch_reason'],
        :_switch_count => fields['switch_count']
    )
  
  ensure
    sleep 600
  end
end